var http = require("http");

var queryString = require("querystring");

var handlebars = require("handlebars");

const formHTML = '<html>' +
    '<head>' +
    '<title>Add something</title>' +
    '<meta charset="utf-8">' +
    '</head>' +
    '<body>' +
    '<form method="post" action="">' +
    '<div>' +
    '<label for="nickname">Nickname:</label>'+
    '<input type="text" name="nickname">' +
    '</div>' +
    '<div>' +
    '<input type="submit" value="send it">' +
    '</div>' +
    '</form>' +
    '</body>' +
    '</html>';

var respText = handlebars.compile("<h1>Your nickname is {{nickname}}</h1>");



var server = http.createServer(function(req,resp){
    console.log("incoming request...");
    if(req.method === "GET") {
        resp.writeHead(200, {
            'Content-Type': 'text/html'
        });
        resp.end(formHTML);
    } else if(req.method === "POST"){
        var requestData = "";
        req.onData(function(data){
            requestData+=data;
        });
        req.onEnd(function(){
            var dataObject = queryString.parse(requestData);
            console.log("nickname received: " + dataObject.nickname);
            resp.writeHead(200, {
                'Content-Type': 'text/html'
            });
            resp.end(respText(dataObject));
        });
    }
}).listen(3000, '127.0.0.1');

console.log("server is listening...")


