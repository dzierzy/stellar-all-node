
var MainView = React.createClass({
    render: function () {

        return (
            <div>
                <header>
                    <nav>
                        <ul>
                            <li><a>Stellar Catalogue</a></li>
                        </ul>
                    </nav>
                </header>
                <article>
                    <section id="content">
                        <Search />
                    </section>
                </article>
            </div>
        );
    }
});

var Search = React.createClass({

    getInitialState: function () {
        return { loaded: false };
    },

    search: function () {

        /*
        var that = this;

        setTimeout(function () {
            that.setState({
                loaded: true,
                data: [{ name: "Andromeda", id: "and" }, { name: "Taurus", id: "tau" }]
            });
        }, 5000);*/

        var queryString = //"ri";
            this.state.query;

        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://localhost:3000" + "/constellations?query=" + queryString, false);
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);

        this.setState({
            loaded: true,
            data: data
        });


    },

    onInputChange: function(event){
        this.setState(
            {
                query: event.target.value
            }
        );
    },

    render: function () {
        return (

            <div>
                <div id="search">
                    <span>Search</span>
                    <div className="controls">
                        <form className="searchform" role="search" method="get" action=".">
                            <div className="surname">
                                <div className="search-container">
                                    <input onChange={this.onInputChange.bind(this)} formControlName="name" id="searchText" autofocus type="search" className="search-input" name="name" defaultValue="search" onfocus="if (this.value == 'search') {this.value = '';}" onblur="if (this.value == '') {this.value = 'search';}" ></input>
                                    <button type="button" role="button" defaultValue="" className="search-submit" name="submit" title="Search"
                                            onClick={this.search}  ></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <Constellations display={this.state.loaded} data={this.state.data} />
            </div>

        );
    }

});

var Constellations = React.createClass({

    getInitialState: function () {
        return { selected: false };
    },

    select: function(id){

        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://localhost:3000" + "/constellations/" + id + "/stars", false);
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);

        this.setState({
            selected: true,
            data: data
        });
    },

    render: function () {

        if (!this.props.display) {
            return null;
        }

        var that = this;

        return (

            <div>

                <div id="dbs">
                    <span>Constellations</span>
                    <ul>
                        { this.props.data.map(function (c) {
                            return <li className="db" id={c.id} onClick={()=>that.select(c.id)}>
                                <span><img src="./img/down.png" /></span><span title=""></span><span>{c.name}</span>
                            </li>;
                        })}
                    </ul>
                </div>
                <Stars display={this.state.selected} data={this.state.data}/>
            </div>

        );
    }

});

var Stars = React.createClass({

    render: function () {

        if (!this.props.display) {
            return null;
        }

        return (

            <div id="data">
                <span>Stars</span>
                <ul>
                    { this.props.data.map(function (s) {
                        return <li className="db">
                            <span><img src="./img/down.png" /></span><span>{s.name}</span>
                        </li>;
                    })}
                </ul>
            </div>

        );
    }

});


ReactDOM.render(<MainView />, document.getElementById("greeting"));






