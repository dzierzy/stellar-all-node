let handlebars = require("handlebars");

const formHTML = '<html>' +
    '<head>' +
    '<title>Add something</title>' +
    '<meta charset="utf-8">' +
    '</head>' +
    '<body>' +
    '<form method="post" action="">' +
    '<div>' +
    '<label for="nickname">Nickname:</label>' +
    '<input type="text" name="nickname">' +
    '</div>' +
    '<div>' +
    '<input type="submit" value="send it">' +
    '</div>' +
    '</form>' +
    '</body>' +
    '</html>';

var respText = handlebars.compile("<h1>Your nickname is {{nickname}}</h1>");

exports.formHTML = formHTML;
exports.respText = respText;