let express = require("express");
let bodyParser = require("body-parser");
let handlebars = require("handlebars");
let cors = require("cors");

let mysql = require("mysql");

let view = require("./view");

let app = express();
app.use(bodyParser.urlencoded());
app.use(cors());
app.use(express.static("public"));


var connection = mysql.createConnection({
    host: "192.168.13.29",
    user: "root",
    password: "mysql",
    database: "stars"
});

connection.connect();


/*app.get("/", function (req, resp) {
    resp.writeHead(200, {
        'Content-Type': 'text/html'
    });
    resp.end(view.formHTML);
});

app.post("/", function (req, resp) {
    var dataObject = req.body;
    console.log(dataObject);
    resp.writeHead(200, {
        'Content-Type': 'text/html'
    });
    resp.end(view.respText(dataObject));
});*/


app.get("/constellations", function(req, resp){

    var query = req.query.query;

    console.info("providing constellation list... query param: " + query);


    connection.query("select * from constellation where name like '%" + query + "%'", function(err, rows){
        if(err){
            console.error("error while fetching constellations");
            resp.status(500);
            resp.json({msg: "error while connecting to database"});
        } else {
            var constellations = rows;

            resp.setHeader("Context-Type", "application/json");
            resp.json(constellations);
        }
    });


});

app.get("/constellations/:id/stars", function (req, resp) {
    console.info("fetching stars of constellation " + req.params.id);

    connection.query("select * from star where constellation_id='" + req.params.id + "'", function(err, rows){
        if(err){
            console.error("error while fetching stars");
            resp.status(500);
            resp.json({msg: "error while connecting to database"});
        } else {
            var stars = rows;
            resp.setHeader("Context-Type", "application/json");
            resp.json(stars);
        }
    });

})



app.listen(3000);
